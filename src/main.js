import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from '@/plugins/vuetify'
import * as fb from 'firebase'
import 'vuetify/dist/vuetify.min.css'
import './stylus/main.styl'

import BuyModalComponent from '@/components/Shared/BuyModal'

Vue.config.productionTip = false
Vue.component('add-buy-modal', BuyModalComponent)

new Vue({
	vuetify,
	router,
	store,
	render: h => h(App),
	created() {
		fb.initializeApp({
			apiKey: 'AIzaSyD04Inw9PXfFFJ0HJkWF9RiRMziQhbjfck',
			authDomain: 'vue-ads-project-753d7.firebaseapp.com',
			databaseURL: 'https://vue-ads-project-753d7.firebaseio.com',
			projectId: 'vue-ads-project-753d7',
			storageBucket: 'vue-ads-project-753d7.appspot.com',
			messagingSenderId: '292382666609',
			appId: '1:292382666609:web:5a8cf7c1566866b9ccd0d0'
		})
		
		fb.auth().onAuthStateChanged(user => {
			if (user) {
				this.$store.dispatch('autoLoginUser', user)
			}
		})
		this.$store.dispatch('fetchAds')
	}
}).$mount('#app')
