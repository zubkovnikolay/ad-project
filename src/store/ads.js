import * as fb from 'firebase'

class Ad {
	constructor({title, description, ownerId = null, imageSrc = '', promo = false, id = null}) {
		this.title = title
		this.description = description
		this.ownerId = ownerId
		this.imageSrc = imageSrc
		this.id = id
		this.promo = promo
	}
}

export default {
	state: {
		ads: []
	},
	mutations: {
		createAd(state, payload) {
			state.ads.push(payload)
		},
		loadAds(state, payload) {
			state.ads = payload
		},
		updateAd(state, {title, description, id}) {
			const ad = state.ads.find(ad => {
				return ad.id === id
			})
			ad.title = title
			ad.description = description
		}
	},
	actions: {
		async fetchAds({commit}) {
			commit('clearError')
			commit('setLoading', true)
			
			const resultAds = []
			
			try {
				let ads = (await fb.database().ref('ads').once('value')).val()
				
				Object.keys(ads).forEach(key => {
					ads[key].id = key
					resultAds.push(new Ad(ads[key]))
				})
				commit('loadAds', resultAds)
			} catch (error) {
				commit('setError', error.message)
			} finally {
				commit('setLoading', false)
			}
		},
		async createAd({commit, getters}, payload) {
			commit('clearError')
			commit('setLoading', true)
			
			try {
				payload.ownerId = getters.user.id
				
				const newAd = new Ad({...payload})
				
				const ad = await fb.database().ref('ads').push(newAd)
				
				const image = payload.image
				
				const imageExt = image.name.slice(image.name.lastIndexOf('.'))
				
				const fileData = await fb.storage().ref(`/ad-project/ads/${ad.key}.${imageExt}`).put(image)
				
				const imageSrc = await fb.storage().ref().child(fileData.ref.fullPath).getDownloadURL()
				
				await fb.database().ref('ads').child(ad.key).update({
					imageSrc
				})
				
				commit('createAd', {...newAd, id: ad.key, imageSrc})
			} catch (error) {
				commit('setError', error.message)
			} finally {
				commit('setLoading', false)
			}
		},
		async updateAd({commit}, {title, description, id}) {
			commit('clearError')
			commit('setLoading', true)
			
			try {
				await fb.database().ref('ads').child(id).update({title, description}).then(() => {
					commit('updateAd', {
						title, description, id
					})
				})
			} catch (error) {
				commit('setError', error.message)
				throw error
			} finally {
				commit('setLoading', false)
			}
		}
	},
	getters: {
		ads(state) {
			return state.ads
		},
		promoAds(state) {
			return state.ads.filter(ad => ad.promo)
		},
		myAds(state, getters) {
			return state.ads.filter(ad => ad.ownerId === getters.user.id)
		},
		adById(state) {
			return adId => {
				return state.ads.find(ad => ad.id === adId)
			}
		}
	}
}