import Vue from 'vue'
import Vuex from 'vuex'
import ads from './ads'
import user from './user'
import common from './common'
import orders from './orders'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
	plugins: [
		createPersistedState({
			paths: ['user']
		})
	],
	modules: {
		ads,
		common,
		user,
		orders
	}
})